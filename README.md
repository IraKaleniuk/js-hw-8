## Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)

    Об'єктна модель документа (HTML), тобто весь вміст сторінки представлений об'єктами, які можна міняти
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

   `el.innerHTML` - це весь html-код, який міститься всередині el
   
   `el.innerText` - це весь текст (без html тегів), який міститься всередині el
4. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

    ```javascript
    document.getElementById("id");
    document.getElementsByClassName("class");
    document.getElementsByTagName("class");
    ```
   Кращі, бо новіші і менше писати:
    ```javascript
    document.querySelector("css-selector");
    document.querySelectorAll("css-selector");
    ```
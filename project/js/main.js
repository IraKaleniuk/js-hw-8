"use strict";
const pList = document.querySelectorAll("p");
for(let p of pList) {
    p.style.backgroundColor = "#ff0000";
}

const  optionList = document.querySelector("#optionsList");
console.log(optionList);
console.log(optionList.parentElement);
if(optionList.hasChildNodes()) {
    for (let el of optionList.childNodes) {
        console.log(`Name: ${el.nodeName}; Type: ${el.nodeType}`);
    }
}

const testParagraph = document.querySelector("#testParagraph");
testParagraph.textContent = 'This is a paragraph';


const li = document.querySelectorAll(".main-header > div > ul > li");
for (let el of li) {
    console.log(el);
    el.className = "nav-item";
}

const sectionTitleList = document.querySelectorAll(".section-title");
for (let el of sectionTitleList) {
    el.classList.remove("section-title");
}
